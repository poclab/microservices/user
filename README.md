# user

[![Docker Repository on Quay](https://quay.io/repository/sidlors/user/status?token=662ed6cc-aee8-4c67-9068-699c070dde98 "Docker Repository on Quay")](https://quay.io/repository/sidlors/user)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-user&metric=alert_status)](https://sonarcloud.io/dashboard?id=sidlors-lab-user)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-user&metric=coverage)](https://sonarcloud.io/dashboard?id=sidlors-lab-user)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-user&metric=ncloc)](https://sonarcloud.io/dashboard?id=sidlors-lab-user)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-user&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=sidlors-lab-user)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-user&metric=security_rating)](https://sonarcloud.io/dashboard?id=sidlors-lab-user)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-user&metric=sqale_index)](https://sonarcloud.io/dashboard?id=sidlors-lab-user)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sidlors-lab-user&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=sidlors-lab-user)
