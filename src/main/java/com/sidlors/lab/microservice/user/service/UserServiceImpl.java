package com.sidlors.lab.microservice.user.service;

import java.util.Date;
import java.util.List;

import com.sidlors.lab.microservice.user.model.User;
import com.sidlors.lab.microservice.user.repository.UserRepository;

import org.apache.commons.collections.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserServiceImpl implements UserService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserRepository userRepository;

	@Override
	public User addUser(User body) {
		return null;
	}

	@Override
	public void deleteUser(String apiKey) {

	}

	@Override
	public List<User> findUsersByStatus(List<String> status) {
		return null;
	}

	@Override
	public List<User> findUsersByTags(List<String> tags) {
		return null;
	}

	@Override
	public User getUserById(Long userId) {
		return null;
	}

	@Override
	public void updateUser(User body) {

	}

	@Override
	public void updateUserWithForm(Long userId, String name, String status) {

	}

	@Override
	public void uploadFile(Long userId, String additionalMetadata, MultipartFile file) {

	}


	

}
