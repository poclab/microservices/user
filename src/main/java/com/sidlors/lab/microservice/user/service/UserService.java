package com.sidlors.lab.microservice.user.service;

import java.util.List;

import com.sidlors.lab.microservice.user.model.User;

import org.springframework.web.multipart.MultipartFile;




public interface UserService {

    public abstract User addUser(User body);
    public abstract void  deleteUser( String apiKey);
    public abstract List<User> findUsersByStatus(java.util.List<String> status);
    public abstract List<User> findUsersByTags( java.util.List<String> tags);
    public abstract com.sidlors.lab.microservice.user.model.User getUserById(Long UserId) ;
    public abstract void updateUser(User body) ;
    public abstract void updateUserWithForm( Long UserId,   String name,   String status);
    public abstract void uploadFile(Long UserId, String additionalMetadata,  MultipartFile file) ;



}
