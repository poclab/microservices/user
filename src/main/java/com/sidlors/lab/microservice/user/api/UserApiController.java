package com.sidlors.lab.microservice.user.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sidlors.lab.microservice.user.model.User;
import com.sidlors.lab.microservice.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-06-21T18:15:33.928Z")

@Controller
public class UserApiController implements UserApi {

    private  ObjectMapper objectMapper;

    private  HttpServletRequest request;
    Logger log = LoggerFactory.getLogger(UserApiController.class);

    
    private  UserService userService;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Autowired
    public void setHttpServletRequest( HttpServletRequest request) {
        this.request = request;
    }

    @Autowired
    public void setUserService( UserService userService) {
        this.userService = userService;
    }
 
    @Override
    public Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<Void> createUser(User body)  {
        
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            userService.addUser(body);
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default UserApi interface so no example is generated");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }


    @Override
    public ResponseEntity<Void> createUsersWithListInput(java.util.List<User> body) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default UserApi interface so no example is generated");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

}
