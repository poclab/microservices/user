package com.sidlors.lab.microservice.user.repository;

import java.util.Optional;

import com.sidlors.lab.microservice.user.model.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	Optional<User> findById(Long Id);
	
	
}
