package com.sidlors.lab.microservice.user.test;

import com.sidlors.lab.microservice.user.UserApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UserApp.class)
@WebAppConfiguration
public class UserAppTest {

    @Test
	public void contextLoads() {

	}

}
