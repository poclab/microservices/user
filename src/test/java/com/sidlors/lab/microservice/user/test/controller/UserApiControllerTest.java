package com.sidlors.lab.microservice.user.test.controller;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sidlors.lab.microservice.user.UserApp;
import com.sidlors.lab.microservice.user.api.UserApiController;
import com.sidlors.lab.microservice.user.model.User;
import com.sidlors.lab.microservice.user.service.UserService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UserApp.class)
@WebAppConfiguration
public class UserApiControllerTest {
	
	private static final Logger log = LoggerFactory.getLogger(UserApiControllerTest.class);

	@InjectMocks
	private UserApiController UserApiController;

	@Mock
	private UserService userService;

	@Mock
	private  ObjectMapper objectMapper;

	@Mock
    private  HttpServletRequest request;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(UserApiController).build();
	}



	@Test
	public void shouldRegisterNewUser() throws Exception {
		
		final User user = new User();
		user.setId(13333L);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(user);
		when(request.getHeader("Accept")).thenReturn("application/json");
		when(userService.addUser(user)).thenReturn(user);
		mockMvc.perform(post("/user")
						.contentType(MediaType.APPLICATION_JSON)
						.header("Content-Type", MediaType.APPLICATION_JSON)
						.content(json))
				.andExpect(status().isAccepted());
	}

	@Test
	public void shouldNotRegisterNewUser() throws Exception {
		
		final User user = new User();
		user.setId(133L);
		String json ="{";
		mockMvc.perform(post("/user")
						.contentType(MediaType.APPLICATION_JSON)
						.header("Content-Type", MediaType.APPLICATION_JSON)
						.content(json))
				.andExpect(status().isBadRequest());
	}


	@Ignore
	public void shouldRegisterNewListOfUser() throws Exception {
		
		List<User> users = new ArrayList<User>(3);
		//user.setId(13333L);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(users);
		when(request.getHeader("Accept")).thenReturn("application/json");
		//when(userService.addUser(user)).thenReturn(user);
		mockMvc.perform(post("/user/createWithList")
						.contentType(MediaType.APPLICATION_JSON)
						.header("Content-Type", MediaType.APPLICATION_JSON)
						.content(json))
				.andExpect(status().isAccepted());
	}

	
}
