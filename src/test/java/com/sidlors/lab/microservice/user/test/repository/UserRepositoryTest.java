package com.sidlors.lab.microservice.user.test.repository;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sidlors.lab.microservice.user.UserApp;
import com.sidlors.lab.microservice.user.model.User;
import com.sidlors.lab.microservice.user.repository.UserRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UserApp.class)
@WebAppConfiguration
public class UserRepositoryTest {

	@Autowired
	private UserRepository userRepository;


	@Test
	public void shouldFindUserById() {

		User stub = getStubUser();
		userRepository.save(stub);

		User found = userRepository.findById(stub.getId()).get();
		assertEquals(stub.getId(), found.getId());
	}

	private User getStubUser() {
		
		User User = new User();
		User.setId(11L);

		
		return User;
	}
}
